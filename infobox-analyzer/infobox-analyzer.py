#!/usr/bin/python3
#
# Written by Amir E. Aharoni.
# aaharoni@wikimedia.org
# infobox-analyzer.py is free software;
# you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.

import argparse
import os
import re

import requests
import yaml
from yaml import CLoader as Loader

from bs4 import BeautifulSoup

output_sep = "\t"
language_data_cache = {}

# Errors and special conditions
special_invalid_input_url = "[Invalid input URL]"
special_main_page = "[Main Page]"
special_unexpected_title = "[Unexpected title]"
special_no_target_article = "[No target article]"

template_types = {
    "infobox_templates": "template_namespace",
    "infobox_modules": "module_namespace",
    "pseudo_infobox_templates": "template_namespace",
}
pnp_pattern = re.compile(r"^<p>(?P<text>.+)\n</p>")


def get_language_data(language_code):
    if language_code not in language_data_cache:
        language_data_cache[language_code] = load_language_data(language_code)

    return language_data_cache[language_code]


def load_language_data(language_code):
    data = {}

    language_filename = "languages/" + language_code + ".yaml"
    if os.path.isfile(language_filename):
        with open(language_filename, "r", encoding="utf-8") as language_file:
            data = yaml.load(language_file, Loader=Loader)

            # Boilerplate returns NoneType
            if type(data) is not dict:
                data = {}

    if "meta" not in data:
        data["meta"] = {}

    api_url = get_api_url(language_code)
    namespace_request_result = requests.get(
        api_url,
        {
            "action": "query",
            "format": "json",
            "meta": "siteinfo",
            "formatversion": "2",
            "siprop": "namespaces",
        }
    ).json()["query"]["namespaces"]
    data["meta"]["template_namespace"] = namespace_request_result["10"]["name"]
    data["meta"]["module_namespace"] = namespace_request_result["828"]["name"]

    for template_type, namespace_type in template_types.items():
        if template_type in data:
            for i, template in enumerate(data[template_type]):
                data[template_type][i] = ":".join((
                    data["meta"][namespace_type],
                    template
                ))

    parse_params = {
        "action": "parse",
        "format": "json",
        "prop": "text",
        "wrapoutputclass": "",
        "disablelimitreport": 1,
        "contentmodel": "wikitext",
        "formatversion": "2",
    }

    parse_params["text"] = "{{int:Pagetitle}}"
    pagetitle_message = pnp_pattern.search(
        requests.get(
            api_url,
            parse_params
        ).json()["parse"]["text"]
    ).group("text")

    data["meta"]["title_pattern"] = re.compile(re.sub(
        r"\$1",
        "(?P<title>.+)",
        pagetitle_message
    ))

    parse_params["text"] = "{{int:Pagetitle-view-mainpage}}"
    data["meta"]["main_page_title"] = pnp_pattern.search(
        requests.get(
            api_url,
            parse_params
        ).json()["parse"]["text"]
    ).group("text")

    return data


def get_api_url(language_code):
    return "https://" + language_code + ".wikipedia.org/w/api.php"


def write_output_line(
    file,
    line_number,
    source_article_url="",
    source_language_code="",
    source_title="",
    source_page_length_b=0,
    namespace=0,
    protocol="",
    form_factor="",
    target_title="",
    target_article_url="",
    target_page_length_b=0,
    infobox_elements_count=0,
    infobox_templates_count=0,
    pseudo_infobox_templates_count=0
):
    # For the column headings
    if line_number == 1:
        line_number = "Line number"
        source_article_url = "Source article URL"
        source_language_code = "Source language code"
        source_title = "Source title"
        source_page_length_b = "Source page length (b)"
        namespace = "Namespace"
        protocol = "Protocol"
        form_factor = "Form factor"
        target_title = "Target title"
        target_article_url = "Target article URL"
        target_page_length_b = "Target length (b)"
        infobox_elements_count = "Infobox elements count"
        infobox_templates_count = "Infobox templates count"
        pseudo_infobox_templates_count = "Pseudo-infobox templates count"

    fields = (
        str(line_number),
        source_article_url,
        source_language_code,
        source_title,
        str(source_page_length_b),
        str(namespace),
        protocol,
        form_factor,
        target_title,
        target_article_url,
        str(target_page_length_b),
        str(infobox_elements_count),
        str(infobox_templates_count),
        str(pseudo_infobox_templates_count),
    )

    file.write(output_sep.join(fields) + "\n")


arg_parser = argparse.ArgumentParser(
    prog="infobox-analyzer",
    description="Convert a list of Wikipedia URLs to another language",
    epilog="""Written by Amir E. Aharoni.
    infobox-analyzer is free software;
    you can redistribute it and/or modify it under the terms of
    the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License,
    or (at your option) any later version.
    """
)
arg_parser.add_argument(
    "input_file_name",
    help="Input file name. A plain text file with a URL on each line."
)
arg_parser.add_argument(
    "target_language",
    help="Target language code."
)
args = arg_parser.parse_args()

input_file_name = args.input_file_name
target_language = args.target_language

language_data = get_language_data(target_language)

target_api_url = get_api_url(target_language)

output_file_name = input_file_name + ".out.tsv"

'''Prepare a regular expression for checking URL validity and properties.
Comments about this regular expression:
1. Wikimedia has been using https pretty much exclusively since 2015,
   but evidently, http URLs may appear in the input lists, so the "s"
   is optional.
2. All languages are supported. The language code is the first thing
   after "//".
3. URLs with and without ".m" are the same article. The ".m" is for
   the mobile web version. So it's optional here, but checked later
   for adding extra info to the output.
'''
wikipedia_url_pattern = re.compile(
    r"^(?P<protocol>https?)://" +
    r"(?P<language_code>[^.]*)" +
    r"(?P<mobile>.m)?" +
    r"\.wikipedia.org/.+"
)

with (
    open(input_file_name, "r", encoding="utf-8") as input_urls,
    open(output_file_name, "w", encoding="utf-8") as found_urls
):
    line_number = 1

    # Write the column titles headings
    write_output_line(found_urls, line_number)

    while True:
        line_number += 1

        source_article_url = input_urls.readline()
        if source_article_url == "":
            break

        source_article_url = source_article_url.rstrip(os.linesep)
        print(str(line_number) + ": " + source_article_url)

        # Check that the URL is generally valid
        url_search = wikipedia_url_pattern.search(source_article_url)
        if url_search is None:
            write_output_line(
                found_urls,
                line_number,
                source_article_url,
                special_invalid_input_url
            )
            continue

        protocol = url_search.group("protocol")
        language_code = url_search.group("language_code")
        if url_search.group("mobile") == ".m":
            form_factor = "Mobile web"
        else:
            form_factor = "Desktop"

        # Find the article title
        title_request = requests.get(source_article_url)
        source_soup = BeautifulSoup(title_request.text, "html.parser")
        source_title_with_trail = source_soup.title.get_text()
        title_search = get_language_data(
            language_code
        )["meta"]["title_pattern"].search(
            source_title_with_trail
        )

        if title_search is None:
            if source_title_with_trail == get_language_data(
                language_code
            )["meta"]["main_page_title"]:
                write_output_line(
                    found_urls,
                    line_number,
                    source_article_url,
                    language_code,
                    special_main_page,
                    special_main_page,  # Like a special namespace
                    protocol,
                    form_factor,
                )
            else:
                write_output_line(
                    found_urls,
                    line_number,
                    source_article_url,
                    language_code,
                    source_title_with_trail,
                    special_unexpected_title,
                    special_unexpected_title,
                    protocol,
                    form_factor,
                )
            continue

        # Valid title found
        source_title = title_search.group("title")

        langlinks_params = {
            "action": "query",
            "format": "json",
            # "langlinks": interlanguage links
            # "info": to get the length (in bytes)
            "prop": "langlinks|info",
            "titles": source_title,
            "redirects": 1,
            "formatversion": "2",
            "llprop": "url",
            "lllang": target_language
        }
        langlinks_request = requests.get(
            get_api_url(language_code),
            langlinks_params
        )
        langlinks_response = langlinks_request.json()

        langlinks_response_page = langlinks_response["query"]["pages"][0]
        if "length" in langlinks_response_page:
            source_page_length_b = langlinks_response_page["length"]
        else:
            source_page_length_b = 0
        namespace = langlinks_response_page["ns"]

        if "langlinks" not in langlinks_response_page:
            write_output_line(
                found_urls,
                line_number,
                source_article_url,
                language_code,
                source_title,
                source_page_length_b,
                namespace,
                protocol,
                form_factor,
                special_no_target_article,
            )
            continue

        langlink_data = langlinks_response_page["langlinks"][0]
        target_title = langlink_data["title"]
        target_url = langlink_data["url"]

        # Try to find whether the article has an infobox *element*
        # by examining classes of HTML elements in the parsed output.
        # See also another check for infobox *templates*.
        target_request = requests.get(target_url)
        target_soup = BeautifulSoup(target_request.text, "html.parser")
        infobox_elements_count = len(target_soup.find_all(class_="infobox"))

        # Find the page's length in bytes and
        # try to find whether the article has an infobox
        # by querying whether it has known infobox templates.
        # Because infoboxes are not uniform across languages,
        # the result may or may not be the same as with
        # checking infobox *elements*.
        target_query_params = {
            "action": "query",
            "format": "json",
            "prop": "templates|info",
            "titles": target_title,
            "redirects": 1,
            "formatversion": "2",
            "tllimit": "max",
        }
        target_query_request = requests.get(
            target_api_url,
            target_query_params
        )
        target_page_json = target_query_request.json()["query"]["pages"][0]
        if "length" in target_page_json:
            target_length_b = target_page_json["length"]
        else:
            target_length_b = 0

        templates_count = {}
        for template_type in template_types:
            templates_count[template_type] = 0

        if "templates" in target_page_json:
            for template in target_page_json["templates"]:
                for template_type in template_types:
                    if (
                        template_type in language_data and
                        template["title"] in language_data[template_type]
                    ):
                        templates_count[template_type] += 1

        write_output_line(
            found_urls,
            line_number,
            source_article_url,
            language_code,
            source_title,
            source_page_length_b,
            namespace,
            protocol,
            form_factor,
            target_title,
            target_url,
            target_length_b,
            infobox_elements_count,
            (
                templates_count["infobox_templates"] +
                templates_count["infobox_modules"]
            ),
            templates_count["pseudo_infobox_templates"]
        )

exit()
