# infobox-analyzer
This is a script that gets a list of Wikipedia article URLs in various languages
and a target language, and produces a report that shows:
* Whether the article has a corresponding article in the target language.
* The target article's title.
* The size in bytes of the source and the target articles.
* Whether the target article has an infobox.

The infoboxes are the most complex part. Because infoboxes are templates,
and templates are messy and, as of 2023,
[impossible to share across languages](https://www.mediawiki.org/wiki/Global_templates),
they have to be manually configured for each target language.
This configuration is done in a yaml file. Templates change all
the time, so a file might get outdated.

Running this script takes about 30 minutes per 1000 articles on my laptop.
Your mileage may vary. The focus was on accuracy and not on performance.
If you can make it faster, patches welcome.

The initial version includes configuration for the Hindi Wikipedia, because
that's what it was made for. If you run it for another language and make
a configuration file for it, please share the YAML file as a patch. Thank you!

## Required modules
* bs4
* pyyaml
* requests

## Usage

python3 infobox-analyzer input_file_name target_language

The input file is a plain text file with a URL on each line.

The target language is specified as the language code that appears in
the URL of the Wikipedia. For example, `hi` for Hindi Wikipedia.

## Some important comments about the output

### Page lengths

The "Source page length (b)" and "Target length (b)" columns show the length
**in bytes**. The unit is really important: because of how Unicode works,
an English letter is usually one byte, while letters in other alphabets
are usually more than three bytes. Cyrillic, Arabic, Greek, and Hebrew letters
are usually two bytes; Devanagari and Bengali letters are usually three bytes;
and so on. So the size of non-Latin articles is even smaller than what may
appear from the numbers themselves.

(It's theoretically possible to measure the number of actual letters,
but for technical reasons, that would take much longer.)

### Infobox counts

There are three columns for infoboxes in the end, and it's important
to understand how they work. This part is slightly technical,
but crucial for understanding:

1. "Infobox elements count": This is the number of *HTML elements* with
the `infobox` CSS class. For technical and historical reasons, actual
infoboxes usually have this class, but some elements that are not actually
infoboxes may have it, too. So if this number is not zero, it doesn't
necessarily mean that the page has an actual infobox. If this number *is* zero,
there are no elements with the `infobox` CSS class in the HTML output of
the page, and most likely, the page doesn't have an infobox. It's still
possible, however, that such a page does have infoboxes if these infobox
templates weren't programmed according to the best practices.
2. "Infobox templates count": This is the number of infobox *templates*
found on the page. A template is identified as an infobox if it appears
in the configuration file for the language. Preparing a list for a language
is the most manual part of the analysis. Some of them are categorized
and some aren't; some of them inherit from a central generic `{{Infobox}}`,
but many don't; some have duplicates with different spelling; etc.
It's worth the effort: if the list is comprehensive, and this number is
non-zero, then it's quite certain that the page has an infobox,
possibly more than one.
3. "Pseudo-infobox templates count": this is the number of templates in
the page, which add the "infobox" CSS class, but which are not actually
infoboxes. These are mostly navigation boxes (or "navboxes" for short) or
little boxes with links to sister projects (Wikibooks, Wikisource,
Commons, etc.), which appear towards the end of the article.

### Tips for creating the infobox list for a new language

If there's no template list for a language, run the script and put its output
in a spreadsheet. Use the spreadsheet's "Filter" function, and find all
the articles that have non-zero elements with the `infobox` class. Examine each
page and try find the infobox template.
1. If you cannot find a template, the page probably has some manual markup
that adds the infobox class. The page should probably be improved to use
a real infobox template.
2. If you find the template, examine the template itself. If the name in
the page is a redirect, don't put the redirect in the file, but the actual
template's page title. Furthermore, check the template's code: is most of it
is based on another template? For example, the template `{{Infobox artist}}`
may be based on `{{Infobox person}}`, which, in turn, may be based on
something like `{{Infobox}}`. If you find a base template, use its title
in the file, because that will cover all the other templates that are based on
it.
3. If you find the template, but it's not actually an infobox, put it in
the "Pseudo infobox templates" section.

## Tips for analysis

Use the "Filter" function in the spreadsheet. Playing with the numbers in
the three "inoboxes" columns can give interesting results. For example,
selecting non-zero in "Infobox elements count" and 0 in the other two columns
shows a list of articles (17 right now) that appear to have an infobox,
but this infobox is just written manually as wikitext as a table within
the page. These pages should be edited by experienced editors, and this
wikitext should be replaced with real infobox templates.

Contrariwise, selecting 0 in "Infobox elements count" and non-zero
in "Infobox templates count" will show articles that do have mostly valid
infobox templates, but the templates' code should be fixed so that they
include the infobox CSS class.
